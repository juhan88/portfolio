resource "aws_s3_bucket" "portfolio" {
  bucket = "juhan-portfolio-stage"
  acl    = "private"
  policy = file("policy.json")
  server_side_encryption_configuration {
    rule {
      apply_server_side_encryption_by_default {        
        sse_algorithm     = "AES256"
      }
    }
  }
  tags = {
    Name = "staging-bucket"
  }
}

output "id" { value = aws_s3_bucket.portfolio.id }
output "arn" { value = aws_s3_bucket.portfolio.arn }
output "bucket_domain_name" { value = aws_s3_bucket.portfolio.bucket_domain_name }