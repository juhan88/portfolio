terraform {
  backend "s3" {
    bucket = "sudobucks-tf-states"
    key    = "portfolio/infra/main"
    region = "us-west-2"
  }
}